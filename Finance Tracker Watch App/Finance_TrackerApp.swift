//
//  Finance_TrackerApp.swift
//  Finance Tracker Watch App
//
//  Created by Azizbek Asadov on 29/12/22.
//

import SwiftUI

@main
struct Finance_Tracker_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
