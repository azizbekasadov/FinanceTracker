//
//  Finance_Tracker_WidgetsBundle.swift
//  Finance Tracker Widgets
//
//  Created by Azizbek Asadov on 29/12/22.
//

import WidgetKit
import SwiftUI

@main
struct Finance_Tracker_WidgetsBundle: WidgetBundle {
    var body: some Widget {
        Finance_Tracker_Widgets()
    }
}
