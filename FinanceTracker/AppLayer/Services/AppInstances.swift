//
//  AppInstances.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//
final class AppInstances {
    static let shared = AppInstances()

    private(set) lazy var storage: StorageService = {
        let coreDataStack = CoreDataStack()
        return CoreDataStorageServiceImpl(coreDataStack: coreDataStack)
    }()

    private init() {}
}
