//
//  StorageService.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import CoreData

protocol Storagable: NSManagedObject {}

protocol StorageService {
    // MARK: - Context (Required by CoreData)

    var context: NSManagedObjectContext { get }

    // - MARK: - Write

    /// Зафиксировать данные лежащие в контексте (в памяти) в БД (на диске)
    func write()

    // MARK: - Fetch

    /// Получить последнюю сохранённую сущность определённого типа
    func fetchLast<T: Storagable>(type: T.Type) -> T?

    /// Получить сущности определённого типа
    func fetch<T: Storagable>(type: T.Type) -> [T]?

    /// Получить сущности определённого типа по предикату
    func fetch<T: Storagable>(type: T.Type, predicate: NSPredicate) -> [T]?

    /// Получить сущности определённого типа сортированные
    func fetch<T: Storagable>(type: T.Type, sortDescriptors: [NSSortDescriptor]) -> [T]?

    /// Получить сущности определённого типа по предикату и сортированные
    func fetch<T: Storagable>(type: T.Type, predicate: NSPredicate, sortDescriptors: [NSSortDescriptor]) -> [T]?

    /// Получить сущности определённого типа с использованием нескольких условий
    func fetch<T: Storagable>(type: T.Type, configuration: FetchConfiguration) -> [T]?

    // MARK: - Remove
    /// Удалить конкретную сущность
    func remove(_ object: Storagable)

    /// Удалить несколько конкретных сущностей
    func remove<S: Sequence>(_ objects: S) where S.Iterator.Element: Storagable

    /// Удалить все сущности соответствующие типу
    func removeAll<T: Storagable>(type: T.Type)

    /// Удалить все сущности соответствующие предикату
    func removeAll<T: Storagable>(type: T.Type, predicate: NSPredicate)
}
