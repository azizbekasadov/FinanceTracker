//
//  CoreDataStack.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import CoreData

/// Warning: Класс для работы с CoreData. Используется через CoreDataStorageImplementation. Не использовать напрямую.
final class CoreDataStack {
    // MARK: - Public vars
    public lazy var context = persistentContainer.viewContext

    // MARK: - Private vars
    private let dataModelName = "FinanceTrackerDB"
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: dataModelName)
        container.loadPersistentStores { _, error in
            if let error = error {
                print("Unable to load persistent stores: \(error)")
            }
        }
        container.viewContext.mergePolicy = NSMergePolicy.overwrite
        return container
    }()

    // MARK: - Public methods
    /// Сохранить данные из памяти на диск.
    func saveContextIfChanged() {
        guard context.hasChanges else { return }
        
        do {
            try context.save()
        } catch {
            print("Unable to save context: \(error)")
        }
    }
}
