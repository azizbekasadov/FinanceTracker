//
//  FinErrors.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

public struct FinErrors: Error {
    public static let cancelledError = FinErrors("", statusCode: -777)
    public static let unknownError = FinErrors("error", statusCode: -777)

    public let message: String
    public let statusCode: Int
    public let originalError: Error?

    public var crashInfo: [String: String] {
        return ["Description": message]
    }

    public init(_ message: String,
         statusCode: Int = -1,
         originalError: Error? = nil) {
        self.message = message
        self.statusCode = statusCode
        self.originalError = originalError
    }
}
