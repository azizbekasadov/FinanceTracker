//
//  Array+Extension.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import Foundation
import CoreGraphics

extension Array where Element: Equatable {
    func removeDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }
}

extension Array {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Array where Element == String? {
    func compactConcate(separator: String) -> String {
        return self.compactMap { $0 }
                .filter { !$0.isEmpty }
                .joined(separator: separator)
    }
}

extension Array where Element == CGPoint {
    // Выстраиваем точки слева направо
    var sortedPoints: [CGPoint] {
        return self.sorted { $0.x < $1.x }
    }
    
    var firstPoint: CGPoint {
        return sortedPoints.first ?? .zero
    }

    var lastPoint: CGPoint {
        return sortedPoints.last ?? .zero
    }
}
