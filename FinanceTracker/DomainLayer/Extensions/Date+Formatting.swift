//
//  Date+Formatting.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import Foundation

/// Только преобразования Date в строку с использованием DateFormatter
extension Date {
    var newsDateFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .dayMonthYearTime)
        return dateFormatter.string(from: self)
    }

    var adDateFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .dayMonthTime)
        return dateFormatter.string(from: self)
    }

    var dayMonthYearDateFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .dayMonthYearShort)
        return dateFormatter.string(from: self)
    }

    var dayMonthYearShortTimeFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .dayMonthYearShortTime)
        return dateFormatter.string(from: self)
    }

    var hourMinuteFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .hourMinute)
        return dateFormatter.string(from: self)
    }

    var minuteSecondFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .minuteSecond)
        return dateFormatter.string(from: self)
    }

    var dayMonthFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .dayMonth)
        return dateFormatter.string(from: self)
    }

    var timeWithMillisecondsFormatString: String {
        let dateFormatter = DateFormatterService.shared.getFormatter(type: .timeWithMilliseconds)
        return dateFormatter.string(from: self)
    }

    /// Если сегодня, то часов назад, если вчера и раньше, то дата
    var relativeDateFormatString: String {
        if self.isToday {
            let hoursAgo = Date().hours(from: self)
            if hoursAgo == 0 {
                var minutesAgo = Date().minutes(from: self)
                if minutesAgo == 0 { minutesAgo = 1 }
                return minutesAgo.stringValue + "minutes"
            }
            return hoursAgo.stringValue + "hours"
        } else {
            return self.dayMonthYearDateFormatString
        }
    }
}
