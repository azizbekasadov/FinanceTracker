//
//  DebugDescription+Extension.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

public extension CustomDebugStringConvertible {
    var debugDescription: String {
        var debudString = "***** \(type(of: self)) *****\n"
        let selfMirror = Mirror(reflecting: self)
        
        for child in selfMirror.children {
            if let propertyName = child.label {
                debudString += "\(propertyName): \(child.value)\n"
            }
        }
        return debudString
    }
}
