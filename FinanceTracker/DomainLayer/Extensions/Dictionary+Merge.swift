//
//  Dictionary+Merge.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

extension Dictionary {
    static func += <K, V> (left: inout [K: V], right: [K: V]) {
        for (k, v) in right {
            left[k] = v
        }
    }
}

extension Dictionary where Key: CustomDebugStringConvertible,
                            Value: CustomDebugStringConvertible {
    var prettyPrint: String {
        self.forEach {
            print("\($0.key) = \($0.value)")
        }
        
        return self.description
    }
}
