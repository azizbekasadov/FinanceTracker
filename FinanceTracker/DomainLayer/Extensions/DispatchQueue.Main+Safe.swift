//
//  DispatchQueue.Main+Safe.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

extension DispatchQueue {
    /// Выполняет блок на главной очереди если мы уже на ней или асинхронно на главной если мы не на ней.
    /// Нужен чтобы уменьшить вероятность гонки состояний.
    /// - Parameter closure: Closure to execute ==> `() -> Void`.
    static func dispatchMainIfNeeded(_ closure: @escaping EmptyClosure) {
        #warning("Apple рекоммендует юзать OperationQueue и другие верхнеуровневые методы")
        guard self === DispatchQueue.main && Thread.isMainThread else {
            DispatchQueue.main.async(execute: closure)
            return
        }

        closure()
    }
}
