//
//  Double+Extension.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

extension Double {
    var date: Date {
        return Date(timeIntervalSince1970: self)
    }
    
    var stringValue: String {
        return String(self)
    }
    
    var intValue: Int {
        Int(self)
    }
}
