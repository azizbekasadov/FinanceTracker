//
//  Int+Extension.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

extension Int {
    var stringValue: String {
        return String(self)
    }
    
    var floatValue: CGFloat {
        return CGFloat(self)
    }
    
    var boolValue: Bool {
        return self == 1
    }
}

extension Int64 {
    var stringValue: String {
        return String(self)
    }
    
    var floatValue: CGFloat {
        return CGFloat(self)
    }
    
    var boolValue: Bool {
        return self == 1
    }
}

extension Int {
    var minimized: String {
        guard self > 1_000 else { return String(self) }
        
        let units: [String] = ["k", "m", "g", "t", "p", "e"]
        var num: Double = Double(self)
        num = fabs(num)
        let exp: Int = Int(log10(num) / 3.0 ) // log10(1000))
        let roundedNum: Double = round(10 * num / pow(1_000.0, Double(exp))) / 10
        
        if floor(roundedNum) == roundedNum {
            return "\(Int(roundedNum))\(units[exp - 1])"
        }
        
        return "\(roundedNum)\(units[exp - 1])"
    }
}
