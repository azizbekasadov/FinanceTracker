//
//  EmptyClosure.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

public typealias EmptyClosure = () -> Void
public typealias Action<Value> = (Value) -> Void
