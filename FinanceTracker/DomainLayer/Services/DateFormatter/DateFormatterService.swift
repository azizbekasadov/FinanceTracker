//
//  AppDateFormatterService.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import Foundation

final class DateFormatterService {
    static let shared: DateFormatterServiceFacade = DateFormatterService()

    private var dayMonthYearTimeFormatter: DateFormatter
    private var dayMonthTimeFormatter: DateFormatter
    private var dayMonthYearShortFormatter: DateFormatter
    private var dayMonthYearShortTimeFormatter: DateFormatter
    private var dayMonthFormatter: DateFormatter
    private var hourMinuteFormatter: DateFormatter
    private var minuteSecondFormatter: DateFormatter
    private var timeWithMillisecondsFormatter: DateFormatter

    private init() {
        dayMonthYearTimeFormatter = DateFormatter()
        dayMonthYearTimeFormatter.dateFormat = "d MMMM yyyy, HH:mm"

        dayMonthTimeFormatter = DateFormatter()
        dayMonthTimeFormatter.dateFormat = "d MMMM, HH:mm"

        dayMonthYearShortFormatter = DateFormatter()
        dayMonthYearShortFormatter.dateFormat = "dd.MM.yyyy"

        dayMonthYearShortTimeFormatter = DateFormatter()
        dayMonthYearShortTimeFormatter.dateFormat = "dd.MM.yyyy HH:mm"

        dayMonthFormatter = DateFormatter()
        dayMonthFormatter.dateFormat = "d MMMM"

        hourMinuteFormatter = DateFormatter()
        hourMinuteFormatter.dateFormat = "HH:mm"

        minuteSecondFormatter = DateFormatter()
        minuteSecondFormatter.dateFormat = "mm:ss"

        timeWithMillisecondsFormatter = DateFormatter()
        timeWithMillisecondsFormatter.dateFormat = "HH:mm:ss.SSS"

        updateLocale()
    }

    private func getLocale() -> Locale {
        var countryCode = Locale.autoupdatingCurrent.identifier

        // для арабского делаем английскую локаль
        if countryCode.contains("ar") {
            countryCode = "en"
        }

        return Locale(identifier: countryCode)
    }
}

extension DateFormatterService: DateFormatterServiceFacade {
    func getFormatter(type: DateFormatterType) -> DateFormatter {
        switch type {
        case .dayMonthYearTime:
            return dayMonthYearTimeFormatter
        case .dayMonthTime:
            return dayMonthTimeFormatter
        case .dayMonthYearShort:
            return dayMonthYearShortFormatter
        case .dayMonthYearShortTime:
            return dayMonthYearShortTimeFormatter
        case .dayMonth:
            return dayMonthFormatter
        case .hourMinute:
            return hourMinuteFormatter
        case .minuteSecond:
            return minuteSecondFormatter
        case .timeWithMilliseconds:
            return timeWithMillisecondsFormatter
        }
    }

    func updateLocale() {
        let locale = getLocale()

        dayMonthYearTimeFormatter.locale = locale
        dayMonthTimeFormatter.locale = locale
        dayMonthYearShortFormatter.locale = locale
        dayMonthYearShortTimeFormatter.locale = locale
        dayMonthFormatter.locale = locale
        hourMinuteFormatter.locale = locale
        timeWithMillisecondsFormatter.locale = locale
    }
}
