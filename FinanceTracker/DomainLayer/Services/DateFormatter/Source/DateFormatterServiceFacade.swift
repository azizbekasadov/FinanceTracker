//
//  DateFormatterServiceFacade.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import Foundation

protocol DateFormatterServiceFacade {
    /// Получить форматтер
    func getFormatter(type: DateFormatterType) -> DateFormatter
    /// Установить новый язык для форматтеров. Берётся из текущей локали.
    /// Вызывается например при смене языка.
    func updateLocale()
}
