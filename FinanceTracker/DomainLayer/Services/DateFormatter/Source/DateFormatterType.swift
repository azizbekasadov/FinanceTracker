//
//  DateFormatterType.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import Foundation

enum DateFormatterType {
    /// 20 february 2017, 00:00
    case dayMonthYearTime

    /// 20 february, 00:00
    case dayMonthTime

    /// 07.04.2017
    case dayMonthYearShort

    /// 07.04.2017, 00:00
    case dayMonthYearShortTime

    /// 10 Февраля
    case dayMonth

    /// 20:40
    case hourMinute

    /// 10:59
    case minuteSecond

    /// 20:40:10.043
    case timeWithMilliseconds
}
