//
//  APIError.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

public enum APIErrorCode {
    static let invalidParameter = 400
    static let badRequest = 401
    static let notFound = 404
    static let alreadyExists = 409
}
