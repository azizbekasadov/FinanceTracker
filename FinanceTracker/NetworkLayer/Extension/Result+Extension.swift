//
//  Result.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

extension Result where Success == Void {
    static var success: Self {
        return .success(Void())
    }
}
