//
//  NetworkTypealiases.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

public typealias TaskCompletion<T> = (Result<T, Error>) -> Void
