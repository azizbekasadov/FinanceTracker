//
//  UIView+Animations.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit

extension UIView {
    func dismiss(animated: Bool, completion: EmptyClosure? = nil) {
        let alreadyHidden = alpha == 0.0 || isHidden
        if !alreadyHidden {
            if animated {
                UIView.animate(withDuration: 0.3,
                               delay: 0.0,
                               options: [.allowUserInteraction, .curveEaseOut],
                               animations: { self.alpha = 0.0 },
                               completion: { _ in self.isHidden = true
                                completion?()
                })
            } else {
                alpha = 0.0
                isHidden = true
                completion?()
            }
        }
    }

    func appear(animated: Bool) {
        let alreadyVisible = alpha == 1.0 && isHidden == false
        if !alreadyVisible {
            if animated {
                if isHidden == true {
                    alpha = 0
                    isHidden = false
                }

                UIView.animate(withDuration: 0.3,
                               delay: 0.0,
                               options: [.allowUserInteraction, .curveEaseOut],
                               animations: { self.alpha = 1.0 },
                               completion: { _ in })
            } else {
                alpha = 1.0
                isHidden = false
            }
        }
    }

    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: center.x - 10, y: center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: center.x + 10, y: center.y))
        layer.add(animation, forKey: "position")
    }

    /// Анимация пульсации
    ///
    /// - Parameters:
    ///   - on: включить/выключить
    ///   - fromValue: начальное значение opacity. От 0 до 1.
    func pulsate(on: Bool, duration: TimeInterval = 0.5,
                    opacityIntensity: CGFloat = 0.0,
                    repeatCount: Float = Float.greatestFiniteMagnitude) {
        let animationKey = "fc_pulse"
        let alreadyAnimated = layer.animation(forKey: animationKey) != nil
        if on {
            guard alreadyAnimated == false else { return }

            let pulseAnimation = CABasicAnimation(keyPath: "opacity")
            pulseAnimation.fromValue = 1
            pulseAnimation.toValue = opacityIntensity
            pulseAnimation.duration = duration
            pulseAnimation.speed = 0.8
            pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            pulseAnimation.autoreverses = true
            pulseAnimation.repeatCount = repeatCount
            layer.add(pulseAnimation, forKey: animationKey)
        } else {
            layer.removeAnimation(forKey: animationKey)
        }
    }

    class func animate(_ duration: TimeInterval,
                       delay: TimeInterval = 0,
                       animations: @escaping EmptyClosure) {
        UIView.animate(
            withDuration: duration,
            delay: delay,
            options: [.curveEaseOut, .allowUserInteraction],
            animations: animations,
            completion: nil
        )
    }

    /// Анимация которая не затормаживает интерфейс
    /// animations - е
    func animateLayoutUpdate(withDuration duration: TimeInterval = 0.25,
                             animations: EmptyClosure? = nil,
                             completion: (Action<Bool>)? = nil) {
        UIView.animate(
            withDuration: duration,
            delay: 0,
            options: [
                .allowUserInteraction,
                .beginFromCurrentState,
                .curveEaseOut
            ],
            animations: {
                animations?()
                self.layoutIfNeeded()
            },
            completion: completion
        )
    }
}
