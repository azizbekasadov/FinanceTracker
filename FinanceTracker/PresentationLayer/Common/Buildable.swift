//
//  Buildable.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import Foundation

protocol Buildable {
    associatedtype ViewController
    
    static func makeScene() -> ViewController
}
