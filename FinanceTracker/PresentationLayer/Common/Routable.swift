//
//  Routable.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

protocol Routable {
    associatedtype Destination
    
    func routeTo(destination: Destination)
}
