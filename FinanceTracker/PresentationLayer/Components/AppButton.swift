//
//  Button.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

final class AppButton: UIButton {
    
    init(style: ButtonStyle) {
        super.init(frame: .zero)
        
        self.setTitleColor(style.textColor, for: .normal)
        self.backgroundColor = style.backgroundColor
        self.tintColor = style.tintColor
        self.layer.cornerRadius = style.corderRadius
        self.titleLabel?.font = style.font
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

struct ButtonStyle {
    public var textColor: UIColor = .link
    public var backgroundColor: UIColor = .clear
    public var corderRadius: CGFloat = 0.0
    public var tintColor: UIColor = .link
    public var font: UIFont = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
    
    static var white: ButtonStyle {
        return ButtonStyle(textColor: UIColor.App.appColor,
                    backgroundColor: UIColor.white,
                    corderRadius: 16.0,
                           tintColor: UIColor.App.appColor)
    }
}
