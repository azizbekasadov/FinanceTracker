//
//  OnboardingView.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

import Stevia
import UIKit
import Lottie

final class OnboardingView: UIView {
    private let titleLabel: StyledLabel = {
        let label = StyledLabel()
        label.textStyle = .headline
            .h0
            .with(color: .label)
            .with(lineBreakMode: .byWordWrapping)
        label.numberOfLines = 0
        label.alpha = 0
        return label
    }()
    
    private let textView: StyledTextView = {
        let tv = StyledTextView(frame: .zero)
        tv.textStyle = .paragraph
            .p3Regular
            .with(color: .label)
            .with(lineBreakMode: .byWordWrapping)
        tv.isScrollEnabled = false
        tv.isUserInteractionEnabled = false
        tv.isEditable = false
        tv.alpha = 0
        return tv
    }()
    
    private let imageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.alpha = 0
        return img
    }()
    
    private lazy var vStackView: UIStackView = {
        let stack = UIStackView.create([titleLabel, textView],
                                       axis: .vertical,
                                       spacing: 10.0)
        stack.distribution = .fill
        stack.alignment = .fill
        return stack
    }()
    
    private(set) var animatedView: LottieAnimationView = {
        let view = LottieAnimationView(name: "gray-shape")
        view.loopMode = .loop
        view.alpha = 0
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        animatedView.play()
    }
    
    private func setupUI() {
        self.subviews(animatedView, vStackView, imageView)
        
        setupLayout()
    }
    
    private func setupLayout() {
        vStackView
            .bottom(20)
            .leading(20)
            .trailing(20)
        
        imageView
            .centerHorizontally()
            .centerVertically(offset: -80)
            .size(200.0)
        
        animatedView
            .size(self.frame.width - 35.0)
        
        animatedView.CenterX == imageView.CenterX
        animatedView.CenterY == imageView.CenterY
    }
    
    func configureWith(item: OnboardingItem) {
        imageView.image = UIImage(named: item.image)
        titleLabel.text = item.title
        textView.text = item.subtitle
    }
    
    func animateViews(completion: (() -> Void)? = nil) {
        let initialState = imageView.transform
        imageView.transform = .init(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options: [.curveEaseOut, .allowAnimatedContent], animations: {
            self.titleLabel.alpha = 1
            self.textView.alpha = 1
            self.imageView.transform = initialState
            self.imageView.alpha = 1
            self.animatedView.alpha = 1
            
            self.layoutIfNeeded()
        }) { _ in
            completion?()
        }
    }
}
