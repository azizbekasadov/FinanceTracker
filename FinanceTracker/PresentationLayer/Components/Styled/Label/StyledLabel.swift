//
//  StyledLabel.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

class StyledLabel: UILabel {
    final var textStyle: TextStyle {
        get {
            return styledText.style
        }
        set {
            styledText.style = newValue
        }
    }

    private final var styledText: StyledText = StyledString(string: "", style: .unspecified) {
        didSet {
            refreshStyle()
        }
    }

    override final var text: String? {
        get {
            return super.attributedText?.string
        }
        set {
            guard let text = newValue else {
                super.attributedText = nil
                return
            }

            styledText = StyledString(string: text, style: textStyle)
        }
    }

    override final var attributedText: NSAttributedString? {
        get {
            return super.attributedText
        }
        set {
            guard let text = newValue else {
                super.attributedText = nil
                return
            }

            styledText = StyledAttributedString(attributedString: text, style: textStyle)
        }
    }

    final func refreshStyle() {
        super.attributedText = styledText.styledAttributedStringValue
    }
}
