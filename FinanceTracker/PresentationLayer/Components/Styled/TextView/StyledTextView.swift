//
//  StyledTextView.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

public final class StyledTextView: UITextView {
    public final var textStyle: TextStyle {
        get {
            return styledText.style
        }
        set {
            styledText.style = newValue
        }
    }

    private final var styledText: StyledText = StyledString(string: "", style: .unspecified) {
        didSet {
            refreshStyle()
        }
    }

    public override final var text: String? {
        get {
            return super.attributedText?.string
        }
        set {
            guard let text = newValue else {
                super.attributedText = nil
                return
            }

            placeholderLabel.isHidden = !text.isEmpty
            styledText = StyledString(string: text, style: textStyle)
        }
    }

    public override final var attributedText: NSAttributedString? {
        get {
            return super.attributedText
        }
        set {
            guard let text = newValue else {
                super.attributedText = nil
                return
            }

            styledText = StyledAttributedString(attributedString: text, style: textStyle)
        }
    }

    public final func refreshStyle() {
        super.attributedText = styledText.styledAttributedStringValue
    }

    // MARK: - Private Properties

    private let placeholderLabel: StyledLabel = StyledLabel()
    private var placeholderLabelConstraints = [NSLayoutConstraint]()

    // MARK: - Public Properties

    public var placeholder: String = "" {
        didSet {
            placeholderLabel.text = placeholder
        }
    }

    public var placeholderStyle: TextStyle = .paragraph.p6Regular {
        didSet {
            setupPlaceholderLabel()
        }
    }

    public override var textAlignment: NSTextAlignment {
        didSet {
            placeholderLabel.textAlignment = textAlignment
        }
    }

    public override var textContainerInset: UIEdgeInsets {
        didSet {
            updateConstraintsForPlaceholderLabel()
        }
    }

    // MARK: - Lifecycle

    override public init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)

        commonInit()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        commonInit()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        placeholderLabel.preferredMaxLayoutWidth = textContainer.size.width - textContainer.lineFragmentPadding * 2.0
    }
}

extension StyledTextView {
    private func commonInit() {
        let textDidChangeNotificationName = UITextView.textDidChangeNotification
        let textDidEndEditingNotificationName = UITextView.textDidEndEditingNotification
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidChange),
                                               name: textDidChangeNotificationName,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidEndEditing),
                                               name: textDidEndEditingNotificationName,
                                               object: nil)

        setupPlaceholderLabel()
        addSubview(placeholderLabel)
        updateConstraintsForPlaceholderLabel()
    }

    private func setupPlaceholderLabel() {
        placeholderLabel.textStyle = placeholderStyle
        placeholderLabel.textAlignment = textAlignment
        placeholderLabel.text = placeholder
        placeholderLabel.numberOfLines = 0
        placeholderLabel.backgroundColor = .clear
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
    }

    private func updateConstraintsForPlaceholderLabel() {
        var newConstraints = createLayoutConstaints()

        newConstraints.append(createLayoutConstraint(item: self, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: placeholderLabel, multiplier: 1.0, constant: textContainerInset.top + textContainerInset.bottom))

        newConstraints.append(createLayoutConstraint(item: placeholderLabel, attribute: .width, relatedBy: .equal, toItem: self, multiplier: 1.0, constant: -(textContainerInset.left + textContainerInset.right + textContainer.lineFragmentPadding * 2.0)))

        removeConstraints(placeholderLabelConstraints)
        addConstraints(newConstraints)
        placeholderLabelConstraints = newConstraints
    }

    @objc
    private func textDidChange() {
        guard let text = text else {
            return
        }

        placeholderLabel.isHidden = !text.isEmpty
    }

    @objc
    private func textDidEndEditing() {
        placeholderLabel.isHidden = !text.isNilOrEmpty
    }

    private func createLayoutConstaints() -> [NSLayoutConstraint] {
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(textContainerInset.left + textContainer.lineFragmentPadding))-[placeholder]",
                                                         options: [],
                                                         metrics: nil,
                                                         views: ["placeholder": placeholderLabel])
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(textContainerInset.top))-[placeholder]",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["placeholder": placeholderLabel])
        return constraints
    }

    private func createLayoutConstraint(item: Any,
                                        attribute: NSLayoutConstraint.Attribute,
                                        relatedBy: NSLayoutConstraint.Relation,
                                        toItem: Any?,
                                        multiplier: CGFloat,
                                        constant: CGFloat) -> NSLayoutConstraint {
        return .init(item: item, attribute: attribute, relatedBy: relatedBy, toItem: toItem, attribute: attribute, multiplier: multiplier, constant: constant)
    }
}

