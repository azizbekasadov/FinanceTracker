//
//  UILabel+Extension.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit

extension UILabel {
    /// Рассчитывает высоту лейбла, в котором бы поместился текст при заданной ширине
    /// - Альтернатива для `intristicSize` of the Content
    func height(constraintedWidth width: CGFloat) -> CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()

        return label.frame.height
    }

    /// Расчитывает количество строк лейбла, в котором бы поместился текст при заданной ширине
    func maxLines(constraintedWidth width: CGFloat) -> Int {
        let maxSize = CGSize(width: width,
                             height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize,
                                         options: .usesLineFragmentOrigin,
                                         attributes: [NSAttributedString.Key.font: font!],
                                         context: nil)
        let linesRoundedUp = Int(ceil(textSize.height / charSize))
        return linesRoundedUp
    }
}
