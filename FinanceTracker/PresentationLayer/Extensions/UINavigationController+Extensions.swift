//
//  UINavigationController+Extensions.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit.UINavigationController
import UIKit.UIViewController

extension UINavigationController {
    func replaceTopViewController(with viewController: UIViewController, animated: Bool) {
        var viewControllers = viewControllers
        viewControllers[viewControllers.count - 1] = viewController
        setViewControllers(viewControllers, animated: animated)
    }
}
