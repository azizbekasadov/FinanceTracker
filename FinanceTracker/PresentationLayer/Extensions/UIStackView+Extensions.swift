//
//  UIStackView+Extensions.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit

extension UIStackView {
    
    /// базовая настройка stackView
    static func create(_ arrangedSubviews: [UIView] = [],
                          axis: NSLayoutConstraint.Axis,
                          spacing: CGFloat) -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = axis
        stackView.spacing = spacing
        return stackView
    }

    func removeAllArrangedSubviews() {
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }

        for v in removedSubviews where v.superview != nil {
            NSLayoutConstraint.deactivate(v.constraints)
            v.removeFromSuperview()
        }
    }
}
