//
//  UIView+Extension.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit

extension UIView {
    /// Огругляет выбранные края.
    ///
    /// - Parameters:
    ///   - corners: UIRectCorner края
    ///   - radius: радиус закругления
    func roundCorners(_ corners: UIRectCorner,
                         radius: CGFloat) {
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
    }
    
    /// Анимирует  кнопку с визуальным увеличением и уменьшением при нажатии
    ///
    /// - Parameters:
    ///   - duration: Double
    ///   - x (coordinate): Double
    ///   - y (coordinate): Double
    public func animate(duration: Double = 0.1,
                        x: Double = 0.93,
                        y: Double = 0.93) {
        UIView.animate(withDuration: duration,
                       animations: {
            self.transform = CGAffineTransform(scaleX: x, y: y)
        }, completion: { _ in
            UIView.animate(withDuration: duration) {
                self.transform = CGAffineTransform.identity
            }
        })
    }
}

