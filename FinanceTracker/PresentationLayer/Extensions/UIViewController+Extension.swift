//
//  UIViewController+Extension.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit
import Stevia

@nonobjc extension UIViewController {
    @discardableResult
    func addChildViewController<T: UIViewController>(_ child: T,
                                            frame: CGRect? = nil,
                                            constraintsConfigurationClosure: ((_ v: UIView) -> Void)?) -> T {
        // First, add the view of the child to the view of the parent
        view.subviews(child.view)
        if let frame = frame {
            child.view.frame = frame
        }
        constraintsConfigurationClosure?(child.view)

        // Then, add the child to the parent
        addChild(child)

        // Finally, notify the child that it was moved to a parent
        child.didMove(toParent: self)

        return child
    }

    func removeFromParent() {
        // First, notify the child that it’s about to be removed
        willMove(toParent: nil)
        // Then, remove the child from its parent
        #warning("Unsafe Recursion: использовать с осторожкой!")
        removeFromParent()
        // Finally, remove the child’s view from the parent’s
        view.removeFromSuperview()
    }
    
    func withNavigation() -> NavigationController {
        return NavigationController(rootViewController: self)
    }
}
