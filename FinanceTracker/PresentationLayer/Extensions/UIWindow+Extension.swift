//
//  UIWindow.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import UIKit

extension UIWindow {
    /// Returns the currently visible view controller if any reachable within the window.
    public var visibleViewController: UIViewController? {
        return UIWindow.visibleViewController(from: rootViewController)
    }
    
    /// Возвращает текущий видимый view controller, находящийся в window.
    public var visibleViewControllerIgnoringBottomTabBar: UIViewController? {
        return UIWindow.visibleViewController(from: rootViewController,
                                              shouldIgnoreBottomTabBar: true)
    }
    
    /// Recursively follows navigation controllers, tab bar controllers and modal presented view controllers starting
    /// from the given view controller to find the currently visible view controller.
    ///
    /// - Parameters:
    ///   - viewController: The view controller to start the recursive search from.
    ///   - shouldIgnoreBottomTabBar: Возвращать BottomTabBarVC (false) или встроенный в него контроллер (true).
    /// - Returns: The view controller that is most probably visible on screen right now.
    public static func visibleViewController(from viewController: UIViewController?,
                                             shouldIgnoreBottomTabBar: Bool = false) -> UIViewController? {
        switch viewController {
        case let navigationController as UINavigationController:
            return UIWindow.visibleViewController(from: navigationController.visibleViewController,
                                                  shouldIgnoreBottomTabBar: shouldIgnoreBottomTabBar)
            
        case let tabBarController as UITabBarController:
            return UIWindow.visibleViewController(from: tabBarController.selectedViewController,
                                                  shouldIgnoreBottomTabBar: shouldIgnoreBottomTabBar)
            
        case let presentingViewController where viewController?.presentedViewController != nil:
            return UIWindow.visibleViewController(from: presentingViewController?.presentedViewController,
                                                  shouldIgnoreBottomTabBar: shouldIgnoreBottomTabBar)
        default:
            return viewController
        }
    }
    
    /// Закрывает все модально представленные контроллеры.
    func closeAllModalControllers(completion: EmptyClosure? = nil) {
        rootViewController?.dismiss(animated: true) {
            completion?()
        }
    }
    
    /// Поочередно закрывает модально представленные контроллеры внутри Navigation Controller.
    private func dismissModalNavigationControllerRecursively() {
        guard let navigationController = rootViewController?.presentedViewController as? UINavigationController else {
            return
        }
        
        navigationController.dismiss(animated: true) { [weak self] in
            self?.dismissModalNavigationControllerRecursively()
        }
    }
}
