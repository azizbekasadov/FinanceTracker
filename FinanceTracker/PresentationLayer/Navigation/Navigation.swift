//
//  Navigation.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import UIKit
import SafariServices

public typealias Identifier = Int

struct Navigation {
    static var keyWindow: UIWindow? = UIApplication.shared.windows.first { $0.isKeyWindow }
    
    static var visibleViewController: UIViewController? {
        if let visibleVC = Navigation.keyWindow?.visibleViewController {
            return visibleVC
        }
        return nil
    }

    static var visibleViewControllerIgnoringBottomTabBar: UIViewController? {
        guard let visibleVC = Navigation.keyWindow?.visibleViewControllerIgnoringBottomTabBar else {
            return nil
        }

        return visibleVC
    }

    fileprivate static var rootNavigationController: UINavigationController? {
        let window = UIApplication.shared.delegate?.window
        return window??.rootViewController as? UINavigationController
    }
}

// MARK: - Navigation Configuration
extension Navigation {
    enum TransitionType {
        struct PresentTransitionConfig {
            let modalPresentationStyle: UIModalPresentationStyle?
            let modalTransitionStyle: UIModalTransitionStyle?
        }

        case push
        case present(config: PresentTransitionConfig?)
    }
    
    /// Основной метод для перехода между контроллерами
    ///
    /// - Parameters:
    ///   - controllerID: Идентификатор контроллера. Берется из enum NavigationIdentifier.
    ///   - type: тип показа контроллера. По умолчанию push.
    ///   - embeddedInNavigationController: Вставлять в navigationController, который поддерживает только портретный режим.
    ///   - tryPresentIfPushingFails: Если type == push и navigationController не оказалось - презентовать ли или нет. По умолчанию да.
    ///   - animated: true если анимированно.
    ///   - sourceVC: контроллер с которого переходим
    ///   - shouldSetAsRoot: устанавливает рутовым контроллером в контейнере
    static func navigate(to controllerID: NavigationIdentifier,
                         type: TransitionType = .push,
                         embeddedInNavigationController: Bool = false,
                         tryPresentIfPushingFails: Bool = true,
                         animated: Bool = true,
                         on sourceVC: UIViewController? = nil,
                         shouldSetAsRoot: Bool = false) {
        guard let vc = instantiateViewController(controllerID) else {
            return
        }

        var resultViewController: UIViewController
        if embeddedInNavigationController {
            let navigationController = NavigationPortraitOnlyController(rootViewController: vc)
            resultViewController = navigationController
        } else {
            resultViewController = vc
        }

        DispatchQueue.dispatchMainIfNeeded {
            switch type {
            case .push:
                if let navigationC = sourceVC?.navigationController ?? rootNavigationController {
                    if shouldSetAsRoot {
                        navigationC.setViewControllers([resultViewController], animated: animated)
                    } else {
                        navigationC.pushViewController(resultViewController, animated: animated)
                    }
                } else if tryPresentIfPushingFails {
                    print("Can't push \(controllerID), trying to present")
                    navigate(to: controllerID,
                             type: .present(config: nil),
                             tryPresentIfPushingFails: false,
                             on: sourceVC)
                } else {
                    print("Failed to push \(controllerID)")
                }
            case .present(let config):
                if let sourceVC = sourceVC ?? visibleViewController {
                    if let presentationStyle = config?.modalPresentationStyle {
                        resultViewController.modalPresentationStyle = presentationStyle
                    }
                    if let transitionStyle = config?.modalTransitionStyle {
                        resultViewController.modalTransitionStyle = transitionStyle
                    }

                    // Настройка делегата для PresentationController
                    if let presentationDelegate = visibleViewControllerIgnoringBottomTabBar
                        as? UIAdaptivePresentationControllerDelegate {
                        resultViewController.presentationController?.delegate = presentationDelegate
                    }

                    sourceVC.present(resultViewController, animated: animated, completion: nil)
                } else {
                    print("Failed to present \(controllerID)")
                }
            }
        }
    }

    static func close(_ wrappedController: UIViewController?) {
        guard let controller = wrappedController else { return }

        if let navVC = controller.navigationController, navVC.viewControllers.count > 1 {
            navVC.popViewController(animated: true)
        } else {
            controller.dismiss(animated: true, completion: nil)
        }
    }
}

extension Navigation {
    /// Основной метод по переходу на главный экран. Main может быть любым. Есть несколько точек входа в него, поэтому сделано отдельным методом.
    static func makeMainAsRoot() {
        make(asRoot: .tabBar)
    }

    static func make(asRoot: RootableNavigationIdentifier) {
        var viewController: UIViewController

        switch asRoot {
        case .splash:
            viewController = SplashBuilder.makeScene()
        case .tabBar:
            viewController = UIViewController()
        case .startScreen:
            viewController = UIViewController() // Splash
        case .tutorial:
            viewController = UIStoryboard(name: "TutorialStory", bundle: nil).instantiateInitialViewController()!
        }

        Navigation.makeAsRoot(viewController: viewController)
    }

    /// Делает контроллер рутовым в окне программы
    fileprivate static func makeAsRoot(viewController: UIViewController) {
        print("Making root \(viewController)")
        DispatchQueue.dispatchMainIfNeeded {
            if let window = Navigation.keyWindow {
                window.rootViewController = viewController
                window.makeKeyAndVisible()
            } else {
                print("FinTracker error: <Navigation> -> The window doesn't exist.")
            }
        }
    }
}

// MARK: -

extension Navigation {
    static func openURL(path: String?, delegate: SFSafariViewControllerDelegate? = nil) {
        guard let url = URL(string: path ?? "") else {
            print("String is not a URL.")
            return
        }

        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            // Can open with SFSafariViewController
            DispatchQueue.dispatchMainIfNeeded { [weak visibleViewController] in
                let sVC = SFSafariViewController(url: url)
                sVC.dismissButtonStyle = .close
                sVC.preferredBarTintColor = UIColor.Palette.backgroundPrimary.uiColor
                sVC.preferredControlTintColor = UIColor.Palette.seaBlue.uiColor
                sVC.delegate = delegate
                visibleViewController?.present(sVC, animated: true, completion: nil)
            }
        } else {
            // Scheme is not supported or no scheme is given, use openURL
            openURLInApp(path: path)
        }
    }

    static func openURLInApp(path: String?) {
        guard let url = URL(string: path ?? "") else {
            print("String is not a URL.")
            return
        }
        guard UIApplication.shared.canOpenURL(url) else {
            print("Can't open URL")
            return
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

// MARK: -

extension NavigationIdentifier: Equatable {
    static func == (lhs: NavigationIdentifier, rhs: NavigationIdentifier) -> Bool {
        return String(describing: lhs) == String(describing: rhs)
    }
}
