//
//  NavigationIdentifier.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import UIKit

enum NotificationNavigationIdentifier {
    case settings
    case onboarding
}

enum NavigationIdentifier {
    case onboarding
    case notification(type: NotificationNavigationIdentifier)
    case addItem(model: Any) // passthrough object of the item data
    case paywall
    case currencyPicker(model: Any) // currencies
    case languagePicker(model: Any) // languages
    case emptyStorage
    case exchangeRates
    case sorting
    case appearance
    case password
    case siriShortcuts
    case sound
    case feedback
    case web(url: URL)
    case att
    case aboutApp
    case alert(model: Any) // AlertModel
}

extension Navigation {
    static func instantiateViewController(_ controllerID: NavigationIdentifier) -> UIViewController? {
        guard let vc = configure(controllerFor: controllerID) else {
            print("Can't instantiate controller \(controllerID)!")
            return nil
        }

        return vc
    }
    
    fileprivate static func configure(controllerFor identifier: NavigationIdentifier) -> UIViewController? {
        switch identifier {
        case .att:
            return AttBuilder.makeScene()
        case .notification(let type):
            switch type {
            case .onboarding:
                return NotificationsBuilder.makeScene()
            case .settings:
                return nil
            }
        case .onboarding:
            let module = OnboardingBuilder.makeScene().withNavigation()
            return module
        case .addItem(let model):
            return nil
        case .paywall:
            return nil
        case .currencyPicker(let model):
            return nil
        case .languagePicker(let model):
            return nil
        case .emptyStorage:
            return nil
        case .exchangeRates:
            return nil
        case .sorting:
            return nil
        case .appearance:
            return nil
        case .password:
            return nil
        case .siriShortcuts:
            return nil
        case .sound:
            return nil
        case .feedback:
            return nil
        case .web(let url):
            return nil
        case .aboutApp:
            return nil
        case .alert(let model):
            return nil
        }
    }
}
