//
//  RootNavigationIdentifiers.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

enum RootableNavigationIdentifier {
    case splash
    case tabBar
    case tutorial
    case startScreen
}
