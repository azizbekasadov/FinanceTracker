//
//  NavigationController.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

import UIKit

final class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func setupUI() {
        view.backgroundColor = UIColor.systemBackground
        
        self.navigationBar.isOpaque = true
        self.navigationBar.isTranslucent = false
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = UIColor.label
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().isOpaque = true
        UINavigationBar.appearance().tintColor = UIColor.label
        UINavigationBar.appearance().shadowImage = UIImage()
    }
}
