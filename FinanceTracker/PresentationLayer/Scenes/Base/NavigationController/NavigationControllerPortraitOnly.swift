//
//  NavigationControllerPortraitOnly.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 27/12/22.
//

import UIKit.UINavigationController

final class NavigationPortraitOnlyController: UINavigationController {
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
