//
//  AttAdapter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 01/01/23.
//

import UIKit

final class AttAdapter: NSObject {
    private weak var collectionView: UICollectionView!
    
    private var items: [String] = []
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init()
        
        setupUI()
    }
    
    private func setupUI() {
        AttCVCell.registerWithoutNib(for: collectionView)
        
        (collectionView.collectionViewLayout as! AttFlowLayout).estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInsetAdjustmentBehavior = .always
    }
    
    func configureWith(items: [String]) {
        self.items = items
        
        collectionView.reloadData()
    }
    
}

extension AttAdapter: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = AttCVCell.dequeue(collectionView, for: indexPath)
        cell.configureWith(title: self.items[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50.0)
    }
}
