//
//  AttBuilder.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

struct AttBuilder: Buildable {
    static func makeScene() -> AttViewController {
        let module = AttViewController()
        loadScene(viewController: module)
        return module
    }
}

extension AttBuilder {
    private static func loadScene(viewController: AttViewController) {
        let interactor = AttInteractor()
        let presenter = AttPresenter()
        let router = AttRouter()

        interactor.presenter = presenter
        presenter.viewController = viewController

        router.viewController = viewController

        viewController.interactor = interactor
        viewController.router = router
    }
}
