//
//  AttInteractor.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

protocol AttBusinessLogic: AnyObject {
    func viewDidLoad(request: AttModels.InitData.Request)
}

final class AttInteractor {
    var presenter: AttPresentationLogic?
}

extension AttInteractor: AttBusinessLogic {
    func viewDidLoad(request: AttModels.InitData.Request) {
        let response = AttModels.InitData.Response(items: [
            "Provide you with a personized  user experience",
            "Help keep this app free of charge",
            "Support developer in the optimization of the application",
            "Provide you with the most accurate and exact forecasts and help our AI calculate factors more precisely",
        ])
        
        presenter?.displayData(response: response)
    }
}
