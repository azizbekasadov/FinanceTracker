//
//  AttModels.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

enum AttModels {
    enum InitData {
        struct Request {}
        
        struct Response {
            let items: [String]
        }
        
        struct ViewModel {
            let items: [String]
        }
    }
}
