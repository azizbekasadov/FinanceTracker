//
//  AttPresenter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

protocol AttPresentationLogic {
    func displayData(response: AttModels.InitData.Response)
}

final class AttPresenter {
    weak var viewController: AttDisplayLogic?
}

extension AttPresenter: AttPresentationLogic {
    func displayData(response: AttModels.InitData.Response) {
        let viewModel = AttModels.InitData.ViewModel(items: response.items)
        viewController?.initData(viewModel: viewModel)
    }
}
