//
//  AttRouter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

protocol AttRoutingLogic: Routable {}

final class AttRouter {
    weak var viewController: AttViewController?
}

extension AttRouter: AttRoutingLogic {
    enum Destination {
        case paywall
        case tabs
    }
    
    func routeTo(destination: AttRouter.Destination) {
        switch destination {
        case .paywall: break
        case .tabs: break
        }
    }
}
