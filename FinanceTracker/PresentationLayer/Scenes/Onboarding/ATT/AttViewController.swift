//
//  AttViewController.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

import UIKit

protocol AttDisplayLogic: AnyObject {
    func initData(viewModel: AttModels.InitData.ViewModel)
}

final class AttViewController: UIViewController {
    private lazy var actionButton: StyledButton = {
        let button = StyledButton(frame: .zero)
        button.alpha = 0
        button.normalTextStyle = .buttons.textButtonBig.with(color: UIColor.white).with(alignment: .center).with(font: UIFont.systemFont(ofSize: 17.0, weight: .medium))
        button.backgroundColor = UIColor.App.appColor
        button.layer.cornerRadius = 12.0
        button.setTitle("Continue", for: .normal)
        button.onTap { [weak self] in
            guard let selfStrong = self else { return }
            
            OperationQueue.main.addOperation {
                selfStrong.actionButton.animate()
            }
        }
        return button
    }()
    
    private let titleLabel: StyledLabel = {
        let lbl = StyledLabel()
        lbl.textStyle = .headline.h0
                                 .with(color: .label)
                                 .with(alignment: .center)
        lbl.numberOfLines = 0
        lbl.text = "You're on iOS 14.5+"
        return lbl
    }()
    
    private let descriptionLabel: StyledLabel = {
        let lbl = StyledLabel()
        lbl.textStyle = .paragraph.p3Regular
                                 .with(color: .label)
                                 .with(alignment: .center)
        lbl.numberOfLines = 0
        lbl.text = "This version of iOS requires us to ask permission to track some date from this device. If you opt it, we can show you relevant content to improve the app experience."
        return lbl
    }()
    
    private lazy var vStackView: UIStackView = {
        let stack = UIStackView.create([titleLabel, descriptionLabel],
                                       axis: .vertical,
                                       spacing: 10.0)
        return stack
    }()
    
    private let imageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.alpha = 0
        img.image = UIImage(named: "puzzle-iso-color")
        return img
    }()
    
//    AttAdapter
    private lazy var collectionView: UICollectionView = {
        let layout = AttFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    private let visualView: UIVisualEffectView = {
        let effect = UIBlurEffect(style: .regular)
        let view = UIVisualEffectView(effect: effect)
        view.alpha = 0
        return view
    }()
    
    private lazy var adapter = AttAdapter(collectionView: collectionView)
    
    var interactor: AttBusinessLogic?
    var router: (any AttRoutingLogic)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateViews {
            self.animateButton()
        }
    }
    
    private func loadData() {
        let request = AttModels.InitData.Request()
        interactor?.viewDidLoad(request: request)
    }
    
    private func setupUI() {
        view.backgroundColor = .systemBackground
        visualView.contentView.subviews(actionButton)
        view.subviews(imageView, vStackView, collectionView, visualView)
        
        setupLayout()
    }
    
    private func setupLayout() {
        imageView
            .centerHorizontally()
            .top(35.0)
            .size(200.0)
        
        vStackView.Top == imageView.Bottom + 20.0
        vStackView
            .leading(20.0)
            .trailing(20.0)
        
        collectionView.Top == vStackView.Bottom + 20.0
        collectionView
            .leading(20.0)
            .trailing(20.0)
            .bottom(0)
        
        actionButton
            .height(50.0)
            .bottom(32.0)
            .top(20.0)
            .leading(20.0)
            .trailing(20.0)
        
        visualView
            .bottom(0)
            .fillHorizontally()
    }
    
    private func animateViews(completion: (() -> Void)? = nil) {
        let initialState = imageView.transform
        imageView.transform = .init(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options: [.curveEaseOut, .allowAnimatedContent], animations: {
            self.titleLabel.alpha = 1
            self.descriptionLabel.alpha = 1
            self.imageView.transform = initialState
            self.imageView.alpha = 1
            
            self.view.layoutIfNeeded()
        }) { _ in
            completion?()
        }
    }
    
    private func animateButton() {
        let actionInitial = self.actionButton.transform
        let visualInitial = self.visualView.transform
        
        self.actionButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.visualView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        UIView.animate(withDuration: 0.35,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options: [.curveEaseOut, .allowAnimatedContent, .showHideTransitionViews]) {
            self.actionButton.alpha = 1
            self.actionButton.transform = actionInitial
            self.visualView.alpha = 1
            self.visualView.transform = visualInitial
            
            self.view.layoutIfNeeded()
        }
    }
}

extension AttViewController: AttDisplayLogic {
    func initData(viewModel: AttModels.InitData.ViewModel) {
        adapter.configureWith(items: viewModel.items)
    }
}
