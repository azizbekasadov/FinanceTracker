//
//  AttCVCell.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 01/01/23.
//

import UIKit
import Stevia

//content:
// 1. Provide you with a personized  user experience
// 2. Help keep this app free of charge
// 3. Support developers to optimize application
// 4. Provide you with the most accurate and exact forecasts and help our AI calculate factors more precisely

final class AttCVCell: UICollectionViewCell {
    private let descriptionLabel: StyledLabel = {
        let lbl = StyledLabel()
        lbl.textStyle = .paragraph.p3Regular
                                 .with(color: .label)
        lbl.numberOfLines = 0
        lbl.text = ""
        lbl.setContentHuggingPriority(.defaultLow, for: .vertical)
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.cornerRadius = 12.0
    }
    
    private func setupUI() {
        contentView.backgroundColor = UIColor.systemGroupedBackground.withAlphaComponent(0.75)
        contentView.subviews(descriptionLabel)
        
        setupLayout()
    }
    
    private func setupLayout() {
        descriptionLabel
            .fillVertically(padding: 12.0)
            .fillHorizontally(padding: 12.0)
    }
    
    func configureWith(title: String?) {
        descriptionLabel.text = title
        
        contentView.layoutIfNeeded()
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        layoutAttributes.frame.size = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
        return layoutAttributes
    }
}
