//
//  NotificationsBuilder.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

struct NotificationsBuilder: Buildable {
    static func makeScene() -> NotificationsViewController {
        let module = NotificationsViewController()
        loadScene(viewController: module)
        return module
    }
}

extension NotificationsBuilder {
    private static func loadScene(viewController: NotificationsViewController) {
        let interactor = NotificationsInteractor()
        let presenter = NotificationsPresenter()
        let router = NotificationsRouter()

        interactor.presenter = presenter
        presenter.viewController = viewController

        router.viewController = viewController

        viewController.interactor = interactor
        viewController.router = router
    }
}
