//
//  NotificationsInteractor.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

protocol NotificationsBusinessLogic: AnyObject {
    func viewDidLoad(request: NotificationsModels.InitData.Request)
    
    func didContinueButtonPress(request: NotificationsModels.ContinueButton.Request)
    
    func didSkipButtonPress(request: NotificationsModels.SkipButton.Request)
}

final class NotificationsInteractor {
    var presenter: NotificationsPresentationLogic?
}

extension NotificationsInteractor: NotificationsBusinessLogic {
    func viewDidLoad(request: NotificationsModels.InitData.Request) {
        let response = NotificationsModels.InitData.Response(onboardingItem: request.onboardingItem)
        presenter?.displayData(response: response)
    }
    
    func didContinueButtonPress(request: NotificationsModels.ContinueButton.Request) {
        let response = NotificationsModels.ContinueButton.Response()
        presenter?.continueButtonPressed(response: response)
    }
    
    func didSkipButtonPress(request: NotificationsModels.SkipButton.Request) {
        let response = NotificationsModels.SkipButton.Response()
        presenter?.skipButtonPressed(response: response)
    }
}
