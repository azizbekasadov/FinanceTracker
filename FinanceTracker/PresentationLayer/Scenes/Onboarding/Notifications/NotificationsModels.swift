//
//  NotificationsModels.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

enum NotificationsModels {
    enum InitData {
        struct Request {
            let onboardingItem: OnboardingItem
        }
        
        struct Response {
            let onboardingItem: OnboardingItem
        }
        
        struct ViewModel {
            let onboardingItem: OnboardingItem
        }
    }
    
    enum ContinueButton {
        struct Request {
            
        }
        
        struct Response {
            
        }
        
        struct ViewModel {
            
        }
    }
    
    enum SkipButton {
        struct Request {
            
        }
        
        struct Response {
            
        }
        
        struct ViewModel {
            
        }
    }
}
