//
//  NotificationsPresenter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

protocol NotificationsPresentationLogic: AnyObject {
    func displayData(response: NotificationsModels.InitData.Response)
    
    func continueButtonPressed(response: NotificationsModels.ContinueButton.Response)
    
    func skipButtonPressed(response: NotificationsModels.SkipButton.Response)
}

final class NotificationsPresenter {
    var viewController: NotificationsDisplayLogic?
}

extension NotificationsPresenter: NotificationsPresentationLogic {
    func displayData(response: NotificationsModels.InitData.Response) {
        let viewModel = NotificationsModels.InitData.ViewModel(onboardingItem: response.onboardingItem)
        viewController?.viewIsReady(viewModel: viewModel)
    }
    
    func continueButtonPressed(response: NotificationsModels.ContinueButton.Response) {
        let viewModel = NotificationsModels.ContinueButton.ViewModel()
        viewController?.handleContinueButtonAction(viewModel: viewModel)
    }
    
    func skipButtonPressed(response: NotificationsModels.SkipButton.Response) {
        let viewModel = NotificationsModels.SkipButton.ViewModel()
        viewController?.handleSkipButtonAction(viewModel: viewModel)
    }
}
