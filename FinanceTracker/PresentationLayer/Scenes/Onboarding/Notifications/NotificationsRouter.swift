//
//  NotificationsRouter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

protocol NotificationsRoutingLogic: Routable { }

final class NotificationsRouter {
    weak var viewController: NotificationsViewController?
}

extension NotificationsRouter: NotificationsRoutingLogic {
    enum Destination {
        case att
        case home
    }
    
    func routeTo(destination: Destination) {
        switch destination {
        case .att:
            Navigation.navigate(to: .att, type: .push, on: self.viewController)
        case .home: break
        }
    }
}
