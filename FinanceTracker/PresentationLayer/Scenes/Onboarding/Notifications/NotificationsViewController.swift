//
//  NotificationsViewController.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

import UIKit

protocol NotificationsDisplayLogic {
    func viewIsReady(viewModel: NotificationsModels.InitData.ViewModel)
    
    func handleContinueButtonAction(viewModel: NotificationsModels.ContinueButton.ViewModel)
    
    func handleSkipButtonAction(viewModel: NotificationsModels.SkipButton.ViewModel)
}

final class NotificationsViewController: UIViewController {
    private let onboardingView = OnboardingView(frame: .zero)
    
    private lazy var actionButton: StyledButton = {
        let button = StyledButton(frame: .zero)
        button.alpha = 0
        button.normalTextStyle = .buttons.textButtonBig.with(color: UIColor.white).with(alignment: .center).with(font: UIFont.systemFont(ofSize: 17.0, weight: .medium))
        button.backgroundColor = UIColor.App.appColor
        button.layer.cornerRadius = 12.0
        button.setTitle("Yes, please", for: .normal)
        button.onTap { [weak self] in
            guard let selfStrong = self else { return }
            
            OperationQueue.main.addOperation {
                selfStrong.actionButton.animate()
                
                let request = NotificationsModels.ContinueButton.Request()
                selfStrong.interactor?.didContinueButtonPress(request: request)
            }
        }
        return button
    }()
    
    private lazy var skipButton: StyledButton = {
        let button = StyledButton(frame: .zero)
        button.alpha = 0
        button.normalTextStyle = .buttons.textButtonBig.with(color: UIColor.App.appColor).with(alignment: .center).with(font: UIFont.systemFont(ofSize: 17.0, weight: .medium))
        button.backgroundColor = .clear
        button.layer.cornerRadius = 12.0
        button.setTitle("Later", for: .normal)
        button.onTap { [weak self] in
            guard let selfStrong = self else { return }
            
            OperationQueue.main.addOperation {
                selfStrong.skipButton.animate()
                
                let request = NotificationsModels.SkipButton.Request()
                selfStrong.interactor?.didSkipButtonPress(request: request)
            }
        }
        return button
    }()
    
    private lazy var buttonsVStack: UIStackView = {
        let stack = UIStackView.create([actionButton, skipButton],
                                       axis: .vertical,
                                       spacing: 10.0)
        stack.distribution = .fillEqually
        return stack
    }()
    
    var interactor: NotificationsBusinessLogic?
    var router: (any NotificationsRoutingLogic)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        onboardingView.animatedView.play()
        onboardingView.animateViews { [weak self] in
            self?.animateButtons()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    private func setupUI() {
        view.backgroundColor = .systemBackground
        
        view.subviews(onboardingView, buttonsVStack)
        
        setupLayout()
    }
    
    private func setupLayout() {
        actionButton
            .height(50.0)
        
        buttonsVStack
            .bottom(32.0)
            .leading(20.0)
            .trailing(20.0)
        
        onboardingView
            .top(0)
            .fillHorizontally()
        onboardingView.Bottom == actionButton.Top - 20.0
        
    }
    
    private func animateButtons() {
        let actionInitial = self.actionButton.transform
        let skipInitial = self.skipButton.transform
        
        self.actionButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.skipButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        UIView.animate(withDuration: 0.35,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options: [.curveEaseOut, .allowAnimatedContent, .showHideTransitionViews]) {
            self.actionButton.alpha = 1
            self.actionButton.transform = actionInitial
            self.skipButton.alpha = 1
            self.skipButton.transform = skipInitial
            
            self.view.layoutIfNeeded()
        }
    }
    
    private func loadData() {
        let request = NotificationsModels.InitData.Request(
            onboardingItem: OnboardingItem(id: 0, title: "Stay tuned!", subtitle: "Allows us send you short notifications about very important events and news about your finances in the app", image: "bell-iso-color")
        )
        interactor?.viewDidLoad(request: request)
    }
    
}

extension NotificationsViewController: NotificationsDisplayLogic {
    func viewIsReady(viewModel: NotificationsModels.InitData.ViewModel) {
        onboardingView.configureWith(item: viewModel.onboardingItem)
        
        UIView.animate(0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func handleContinueButtonAction(viewModel: NotificationsModels.ContinueButton.ViewModel) {
        
    }
    
    func handleSkipButtonAction(viewModel: NotificationsModels.SkipButton.ViewModel) {
        (router as? NotificationsRouter)?.routeTo(destination: .att)
    }
}
