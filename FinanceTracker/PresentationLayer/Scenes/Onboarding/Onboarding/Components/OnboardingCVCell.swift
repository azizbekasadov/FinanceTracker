//
//  OnboardingCVCell.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 30/12/22.
//

import Lottie
import Stevia
import UIKit

final class OnboardingCVCell: UICollectionViewCell {
    private let onboardingView = OnboardingView(frame: .zero)
    
    private var item: OnboardingItem!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        onboardingView.animateViews()      
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        contentView.subviews(onboardingView)
        
        setupLayout()
    }
    
    private func setupLayout() {
        onboardingView.fillContainer()
    }
    
    func configureWith(item: OnboardingItem) {
        self.item = item
        self.onboardingView.configureWith(item: item)
    }
}
