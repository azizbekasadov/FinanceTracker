//
//  OnboardingAdapter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

protocol OnboardingAdapterDelegate: AnyObject {
    func didChangeCurrentVisibleItem(_ collectionView: UICollectionView, currentItem index: Int)
}

final class OnboardingAdapter: NSObject {
    private weak var collectionView: UICollectionView!

    private var items: [OnboardingItem] = []
    
    weak var delegate: OnboardingAdapterDelegate?
    
    public init(_ collectionView: UICollectionView, delegate: OnboardingAdapterDelegate? = nil) {
        self.collectionView = collectionView
        self.delegate = delegate
        super.init()
        
        configure()
    }
    
    private func configure() {
        OnboardingCVCell.registerWithoutNib(for: collectionView)
        collectionView.contentInset = UIEdgeInsets.zero
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
    }
    
    public func updateItems(with items: [OnboardingItem]) {
        self.items = items
        
        collectionView.reloadData()
    }
}

extension OnboardingAdapter: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                               numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = OnboardingCVCell.dequeue(collectionView, for: indexPath)
        cell.configureWith(item: items[indexPath.item])
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageIndex = Int(scrollView.contentOffset.x / collectionView.frame.size.width)
        delegate?.didChangeCurrentVisibleItem(collectionView, currentItem: pageIndex)
    }
}

extension OnboardingAdapter: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width,
                      height: collectionView.frame.height)
    }
}
