//
//  OnboardingBuilder.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 31/12/22.
//

import Foundation

struct OnboardingBuilder: Buildable {
    static func makeScene() -> OnboardingViewController {
        let viewController = OnboardingViewController()
        loadScene(viewController: viewController)
        return viewController
    }
}

extension OnboardingBuilder {
    private static func loadScene(viewController: OnboardingViewController) {
        let interactor = OnboardingInteractor()
        let presenter = OnboardingPresenter()
        let router = OnboardingRouter()

        interactor.presenter = presenter
        presenter.viewController = viewController

        router.viewController = viewController

        viewController.interactor = interactor
        viewController.router = router
    }
}
