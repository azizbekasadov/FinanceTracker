//
//  OnboardingInterator.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

protocol OnboardingBusinessLogic {
    func viewDidLoad(request: OnboardingModels.InitData.Request)
    func moveItemTo(request: OnboardingModels.ActionButton.Request)
}

final class OnboardingInteractor {
    var presenter: OnboardingPresentationLogic?
    
    init() {}
}

extension OnboardingInteractor: OnboardingBusinessLogic {
    func viewDidLoad(request: OnboardingModels.InitData.Request) {
        let response = OnboardingModels.InitData.Response(store: OnboardingStore.default)
        presenter?.displayData(response: response)
    }
    
    func moveItemTo(request: OnboardingModels.ActionButton.Request) {
        let response = OnboardingModels.ActionButton.Response(currentPage: request.currentPage)
        presenter?.moveItemTo(response: response)
    }
} 
