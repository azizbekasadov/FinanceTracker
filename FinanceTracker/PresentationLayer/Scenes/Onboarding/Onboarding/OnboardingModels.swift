//
//  OnboardingModels.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

enum OnboardingModels {
    enum InitData {
        struct Request {}
        
        struct Response {
            let store: OnboardingStore
        }
        
        struct ViewModel {
            let store: OnboardingStore
        }
    }
    
    enum ActionButton {
        struct Request {
            let currentPage: Int
        }
        
        struct Response {
            let currentPage: Int
        }
        
        struct ViewModel {
            let currentPage: Int
        }
    }
}
