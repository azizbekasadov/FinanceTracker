//
//  OnboardingPresenter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import Foundation

protocol OnboardingPresentationLogic {
    func displayData(response: OnboardingModels.InitData.Response)
    func moveItemTo(response: OnboardingModels.ActionButton.Response)
}

final class OnboardingPresenter {
    weak var viewController: OnboardingDisplayLogic?
}

extension OnboardingPresenter: OnboardingPresentationLogic {
    func displayData(response: OnboardingModels.InitData.Response) {
        let viewModel = OnboardingModels.InitData.ViewModel(store: response.store)
        viewController?.initialSetup(viewModel: viewModel)
    }
    
    func moveItemTo(response: OnboardingModels.ActionButton.Response) {
        let viewModel = OnboardingModels.ActionButton.ViewModel(currentPage: response.currentPage)
        viewController?.didMoveItemTo(viewModel: viewModel)
    }
}
