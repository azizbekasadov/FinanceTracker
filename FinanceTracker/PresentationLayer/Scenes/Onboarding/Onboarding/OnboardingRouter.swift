//
//  OnboardingRouter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

protocol OnboardingRoutingLogic: Routable {}

final class OnboardingRouter {
    weak var viewController: OnboardingViewController?
}

extension OnboardingRouter: OnboardingRoutingLogic {
    enum Destination {
        case paywall
        case permission
        case notifications
        case tabbar
    }
    
    func routeTo(destination: Destination) {
        switch destination {
        case .paywall: break
        case .permission: break
        case .notifications:
            Navigation.navigate(to: .notification(type: .onboarding),
                                type: .push,
                                on: self.viewController,
                                shouldSetAsRoot: true)
        case .tabbar: break
        }
    }
}

