//
//  OnboardingViewController.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

protocol OnboardingDisplayLogic: AnyObject {
    func initialSetup(viewModel: OnboardingModels.InitData.ViewModel)
    func displayError(viewModel: OnboardingModels.InitData.ViewModel)
    func toggleLoading(viewModel: OnboardingModels.InitData.ViewModel)
    func didMoveItemTo(viewModel: OnboardingModels.ActionButton.ViewModel)
}

// Contents:
// 1. Wallet -> Track your expenses
// 2. Statistics -> Analyse your expenses and income;
// 3. Plan your expenses with more confidence
// 4. Notifications
// 5. ATT
// 6. Paywall
// 7. Home (Tabs)


final class OnboardingViewController: UIViewController {
    var interactor: OnboardingBusinessLogic?
    var router: (any OnboardingRoutingLogic)?
    
    private lazy var collectionView: UICollectionView = {
        let layout = CenteringCollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    private lazy var actionButton: StyledButton = {
        let button = StyledButton(frame: .zero)
        button.normalTextStyle = .buttons.textButtonBig.with(color: UIColor.white).with(alignment: .center).with(font: UIFont.systemFont(ofSize: 17.0, weight: .medium))
        button.backgroundColor = UIColor.App.appColor
        button.layer.cornerRadius = 12.0
        button.setTitle("Continue", for: .normal)
        button.onTap { [weak self] in
            guard let selfStrong = self else { return }
            
            OperationQueue.main.addOperation {
                selfStrong.actionButton.animate()
            }
            
            let current = selfStrong.control.currentPage
            let isLastItem = current == selfStrong.control.numberOfPages - 1
            let currentPage = isLastItem ? current : current + 1
            
            let request = OnboardingModels.ActionButton.Request(currentPage: currentPage)
            selfStrong.interactor?.moveItemTo(request: request)
            
            if isLastItem {
                (selfStrong.router as? OnboardingRouter)?.routeTo(destination: .notifications)
            }
        }
        return button
    }()
    
    private lazy var control: UIPageControl = {
        let control = UIPageControl(frame: .zero)
        control.currentPageIndicatorTintColor = UIColor.App.appColor
        control.pageIndicatorTintColor = UIColor.lightGray
        control.backgroundStyle = .automatic
        control.isUserInteractionEnabled = false
        return control
    }()
    
    private lazy var adapter = OnboardingAdapter(collectionView, delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        setupUI()
    }
    
    private func loadData() {
        let request = OnboardingModels.InitData.Request()
        interactor?.viewDidLoad(request: request)
    }
    
    private func setupUI() {
        view.backgroundColor = UIColor.App.backgroundColor
        
        view.subviews(collectionView, actionButton, control)
        
        setupLayout()
    }
    
    private func setupLayout() {
        actionButton
            .height(50.0)
            .bottom(32.0)
            .leading(20.0)
            .trailing(20.0)
        
        control.Bottom == actionButton.Top - 20.0
        control
            .centerHorizontally()
            .height(20.0)
        
        collectionView.Bottom == control.Top - 20.0
        collectionView
            .fillHorizontally()
            .top(0)
    }
    
    private func changeTitleForButton(at currentPage: Int) {
        let buttonTitle: String = currentPage == control.numberOfPages - 1 ? "Let's start" : "Continue"
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut, .allowAnimatedContent]) {
            self.actionButton.setTitle(buttonTitle, for: .normal)
            self.view.layoutIfNeeded()
        }
    }
}

extension OnboardingViewController: OnboardingDisplayLogic {
    func initialSetup(viewModel: OnboardingModels.InitData.ViewModel) {
        let store = viewModel.store
        control.currentPage = 0
        control.numberOfPages = store.count
        adapter.updateItems(with: store.items)
    }
    
    func displayError(viewModel: OnboardingModels.InitData.ViewModel) {
        
    }
    
    func toggleLoading(viewModel: OnboardingModels.InitData.ViewModel) {
        
    }
    
    func didMoveItemTo(viewModel: OnboardingModels.ActionButton.ViewModel) {
        let currentPage = viewModel.currentPage
        control.currentPage = currentPage
        
        let indexPath = IndexPath(item: currentPage, section: 0)
        collectionView.selectItem(at: indexPath,
                                  animated: true,
                                  scrollPosition: .centeredHorizontally)
        
        changeTitleForButton(at: currentPage)
    }
}

extension OnboardingViewController: OnboardingAdapterDelegate {
    func didChangeCurrentVisibleItem(_ collectionView: UICollectionView,
                                     currentItem index: Int) {
        control.currentPage = index
        changeTitleForButton(at: index)
    }
}
