//
//  OnboardingItem.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 30/12/22.
//

import Foundation

struct OnboardingItem: Identifiable {
    let id: Int
    let title: String
    let subtitle: String
    let image: String
}
