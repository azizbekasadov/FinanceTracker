//
//  OnboardingStore.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 30/12/22.
//

import Foundation

final class OnboardingStore {
    var items: [OnboardingItem] = []
    
    var count: Int {
        items.count
    }
    
    init(items: [OnboardingItem]) {
        self.items = items
    }
    
    static let `default`: OnboardingStore = {
        let items = [
            OnboardingItem(id: 0, title: "Track your expenses with ease", subtitle: "Log your income and outcome, and track your finances in one app", image: "wallet-iso-color"),
            OnboardingItem(id: 1, title: "Analyse your expenses and get statistics", subtitle: "The app will help you track any changes in a simple graphical format", image: "chart-iso-color"),
            OnboardingItem(id: 2, title: "Plan your finances, and save for your dreams", subtitle: "Want a new car? Travel several times a year? Buy a big house? Plan your finances and get modern techniques of saving money", image: "bag-iso-color"),
        ]
        return OnboardingStore(items: items)
    }()
}

//chart-iso-color
//bag-iso-color
