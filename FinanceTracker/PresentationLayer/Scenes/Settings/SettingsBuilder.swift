//
//  SettingsBuilder.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit.UIViewController

struct SettingsBuilder: Buildable {
    static func makeScene() -> UIViewController {
        UIViewController()
    }
}
