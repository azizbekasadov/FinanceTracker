//
//  SettingsRouter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import Foundation

protocol SettingsRoutingLogic: Routable {}

final class SettingsRouter {
    // MARK: - Public properties
    weak var viewController: SettingsViewController?
}

// MARK: - Routing Logic
extension SettingsRouter: SettingsRoutingLogic {
    
    // MARK: Destinations
    enum Destination {
        case paywall
        case benefits
        case templates
        case sorting
        case statistics
        case security
        case appearance
        case notifications
        case siri
        case language
        case feedback
        case rate
        case terms
        case privacy
        case about
    }
    
    func routeTo(destination: Destination) {
        switch destination {
        case .paywall: break
        case .benefits: break
        case .templates: break
        case .sorting: break
        case .statistics: break
        case .security: break
        case .appearance: break
        case .notifications: break
        case .siri: break
        case .language: break
        case .feedback: break
        case .rate: break
        case .terms: break
        case .privacy: break
        case .about: break
        }
    }
}
