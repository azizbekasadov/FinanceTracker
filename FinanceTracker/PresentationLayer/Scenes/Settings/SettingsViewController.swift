//
//  SettingsViewController.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

protocol SettingsDisplayLogic: AnyObject {
    
}

final class SettingsViewController: UIViewController {
    
}

extension SettingsViewController: SettingsDisplayLogic {}
