//
//  SpashViewController.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit
import Lottie

protocol SplashDisplayLogic: AnyObject {
    func initialSetup(viewModel: SplashModels.InitData.ViewModel)
    func displayError(viewModel: SplashModels.InitData.ViewModel)
    func toggleLoading(viewModel: SplashModels.InitData.ViewModel)
}

final class SplashViewController: UIViewController {
    var interactor: SplashBusinessLogic?
    var router: (any SplashRoutingLogic)?
    
    private var bgImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.image = UIImage(named: "launch_bg")
        return img
    }()
    
    private let animatedView: LottieAnimationView = {
        let view = LottieAnimationView(name:"3d-coin")
        view.loopMode = .loop
        view.contentMode = .scaleAspectFill
        view.scalesLargeContentImage = true
        return view
    }()
    
    private let welcomeLabel: StyledLabel = {
        let lbl = StyledLabel()
        lbl.textStyle = .headline.h2
        lbl.textColor = .white
        lbl.numberOfLines = 0
        lbl.text = "Loading..."
        lbl.textAlignment = .center
        return lbl
    }()
    
//    private var coinViews: [UIImageView] = {
//        let images = ["yuan", "rupee", "pound", "euro", "dollar"].map { UIImage(named: $0)}
//
//        var imageViews: [UIImageView] = []
//        images.forEach {
//            let imageView = UIImageView()
//            imageView.contentMode = .scaleAspectFit
//            imageView.image = $0
//            imageViews.append(imageView)
//            imageView.alpha = 0
//        }
//        return imageViews
//    }()
//
//    private lazy var continueButton: AppButton = {
//        let button = AppButton(style: .white)
//        button.setTitle("Continue", for: .normal)
//        button.alpha = 0
//        return button
//    }()
//
//    private let welcomeLabel: StyledLabel = {
//        let lbl = StyledLabel()
//        lbl.textStyle = .headline.h0
//        lbl.textColor = .white
//        lbl.numberOfLines = 0
//        lbl.text = "Finance Tracker v1.0.0"
//        lbl.alpha = 0
//        lbl.textAlignment = .center
//        return lbl
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        animateCoins()
        animatedView.play()
        welcomeLabel.pulsate(on: animated, duration: 0.9)
        
        toggleLoading(viewModel: SplashModels.InitData.ViewModel())
    }
    
    private func setupUI() {
        var _subviews: [UIView] = []
        _subviews.append(bgImageView)
        _subviews.append(animatedView)
//        coinViews.forEach { imgView in
//            _subviews.append(imgView)
//        }
//
        _subviews.append(welcomeLabel)
        view.subviews(_subviews)
        
        setupLayout()
    }
    
    private func setupLayout() {
        bgImageView.fillContainer()
        
        animatedView
            .centerInContainer()
            .size(200.0)
//        coinViews.forEach { imgView in
//            imgView
//                .centerHorizontally()
//                .centerVertically(offset: UIScreen.main.bounds.height * 0.25)
//            imgView.size(150.0)
//        }
//
        welcomeLabel
            .bottom(50.0)
            .centerHorizontally()

        if Device.isPad {
            welcomeLabel.width(250.0)
        } else {
            welcomeLabel
                .leading(20.0)
                .trailing(20.0)
        }
    }
    
//    private func animateCoins() {
//        let transitionOptions: UIView.AnimationOptions = [.curveEaseOut, .beginFromCurrentState]
//
//        let offset: CGFloat = -75.0
//        for index in 0..<coinViews.count {
//            let originalTransform = coinViews[index].transform
//            let scaledTransform = originalTransform.scaledBy(x: 1.2, y: 1.2)
//            let y: CGFloat = offset * CGFloat(index + 1)
//            let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: y)
//            UIView.animate(withDuration: 3.0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: transitionOptions, animations: {
//                self.coinViews[index].transform = index == self.coinViews.count ? originalTransform : scaledAndTranslatedTransform
//                self.coinViews[index].alpha = 1
//                self.view.layoutIfNeeded()
//            }) { (_) in
//                UIView.animate(withDuration: 5.0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: transitionOptions) {
//                    self.welcomeLabel.alpha = 1
//                }
//            }
//        }
//    }
}

extension SplashViewController: SplashDisplayLogic {
    func initialSetup(viewModel: SplashModels.InitData.ViewModel) {
        
    }
    
    func displayError(viewModel: SplashModels.InitData.ViewModel) {
        
    }
    
    func toggleLoading(viewModel: SplashModels.InitData.ViewModel) {
        // MOCk
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            (self.router as? SplashRouter)?.routeTo(destination: SplashRouter.Destination.onboarding)
        }
    }
}
