//
//  SplashBuilder.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

struct SplashBuilder: Buildable {
    static func makeScene() -> SplashViewController {
        let moduleViewController = SplashViewController()
        self.loadScene(viewController: moduleViewController)
        return moduleViewController
    }
}

extension SplashBuilder {
    private static func loadScene(viewController: SplashViewController) {
        let worker = SplashWorker()
        let interactor = SplashInteractor(worker: worker)
        let presenter = SplashPresenter()
        let router = SplashRouter()
        
        interactor.presenter = presenter
        presenter.viewController = viewController

        router.viewController = viewController

        viewController.interactor = interactor
        viewController.router = router
    }
}
