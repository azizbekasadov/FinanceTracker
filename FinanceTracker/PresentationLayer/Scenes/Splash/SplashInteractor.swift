//
//  SplashInteractor.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import Foundation

protocol SplashBusinessLogic: AnyObject {
    func viewDidLoad(request: SplashModels.InitData.Request)
}

final class SplashInteractor {
    var presenter: SplashPresentationLogic?
    
    private var worker: SplashWorkingLogic
    
    init(worker: SplashWorkingLogic) {
        self.worker = worker
    }
}

extension SplashInteractor: SplashBusinessLogic {
    func viewDidLoad(request: SplashModels.InitData.Request) {
        
    }
}
