//
//  SplashModels.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

enum SplashModels {
    enum InitData {
        struct Request {}
        
        struct Response {}
        
        struct ViewModel {}
    }
}
