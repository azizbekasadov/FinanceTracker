//
//  SplashPresenter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import Foundation

protocol SplashPresentationLogic: AnyObject {
    func displayData(response: SplashModels.InitData.Response)
}

final class SplashPresenter {
    weak var viewController: SplashDisplayLogic?
}

extension SplashPresenter: SplashPresentationLogic {
    func displayData(response: SplashModels.InitData.Response) {
        
    }
}
