//
//  SplashRouter.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

protocol SplashRoutingLogic: Routable {}

final class SplashRouter {
    // MARK: - Public Properties
    weak var viewController: SplashViewController?
}

// MARK: - Routing Logic
extension SplashRouter: SplashRoutingLogic {
    // MARK: Destinations
    enum Destination {
        case onboarding
        case tutorial
        case tabbar
    }
    
    func routeTo(destination: Destination) {
        switch destination {
        case .onboarding:
            let configurations = Navigation.TransitionType.PresentTransitionConfig(modalPresentationStyle: .fullScreen, modalTransitionStyle: .crossDissolve)
            Navigation.navigate(to: .onboarding, type: .present(config: configurations))
        case .tutorial:
            break
        case .tabbar:
            break
        }
    }
}
