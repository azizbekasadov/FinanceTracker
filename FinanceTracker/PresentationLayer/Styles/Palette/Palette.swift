//
//  Palette.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit.UIColor

extension UIColor {
    enum Palette {
        enum Accent: String, CaseIterable {
            case blue
            case green
            case red
            
            var uiColor: UIColor {
                return UIColor(named: "accent\(self.rawValue.capitalized)")!
            }
        }
        
        case accent(Accent)
        case backgroundPrimary // backgroundPrimary
        case blackHard
        case text
        case greyBlue
        case greyHard
        case greyLight
        case greyMiddle
        case mintGreen
        case seaBlue
        case teaRose
        case whiteGradient
        case whiteHard
        case scampi
        case chetwoodBlue
        case bokaraGray
        case bastille
        case nightRider
        
        var uiColor: UIColor {
            switch self {
            case let .accent(type): return type.uiColor
            case .backgroundPrimary: return UIColor(named: "backgroundPrimary")!
            case .blackHard: return UIColor(named: "blackHard")!
            case .text: return UIColor.label
            case .greyBlue: return UIColor(named: "greyBlue")!
            case .greyHard: return UIColor(named: "greyHard")!
            case .greyLight: return UIColor(named: "greyLight")!
            case .greyMiddle: return UIColor(named: "greyMiddle")!
            case .mintGreen: return UIColor(named: "mintGreen")!
            case .seaBlue: return UIColor(named: "seaBlue")!
            case .teaRose: return UIColor(named: "teaRose")!
            case .whiteGradient: return UIColor(named: "whiteGradient")!
            case .whiteHard: return UIColor(named: "whiteHard")!
            case .scampi: return UIColor(named: "scampi")!
            case .chetwoodBlue: return UIColor(named: "chetwoodBlue")!
            case .bokaraGray: return UIColor(named: "bokaraGray")!
            case .bastille: return UIColor(named: "bastille")!
            case .nightRider: return UIColor(named: "nightRider")!
            }
        }
    }
    
    struct App {
        static var backgroundColor: UIColor {
            return UIColor.systemBackground
        }
        
        static var appColor: UIColor {
            return UIColor(named: "seaBlue")!
        }
    }
}
