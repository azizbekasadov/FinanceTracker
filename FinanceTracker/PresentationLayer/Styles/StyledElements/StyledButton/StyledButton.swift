//
//  StyledButton.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 30/12/22.
//

import UIKit

public class StyledButton: UIButton {
    // MARK: Public Properties

    public var normalTextStyle: TextStyle = .unspecified {
        didSet {
            updateStyles()
        }
    }
    public var highlightedTextStyle: TextStyle? {
        didSet {
            updateStyles()
        }
    }
    public var disabledTextStyle: TextStyle? {
        didSet {
            updateStyles()
        }
    }
    public var selectedTextStyle: TextStyle? {
        didSet {
            updateStyles()
        }
    }
    public var hideTitle = false {
        didSet {
            updateStyles()
        }
    }

    // MARK: Private Properties

    private var normalTitle: String? {
        didSet {
            updateStyles()
        }
    }
    private var highlightedTitle: String? {
        didSet {
            updateStyles()
        }
    }
    private var disabledTitle: String? {
        didSet {
            updateStyles()
        }
    }
    private var selectedTitle: String? {
        didSet {
            updateStyles()
        }
    }

    // MARK: - Public methods

    final public  func refreshStyle() {
        updateStyles()
    }

    // MARK: - Overrides

    public override final func title(for state: UIControl.State) -> String? {
        if state.contains(.normal) {
            return normalTitle
        } else if state.contains(.highlighted) {
            return highlightedTitle
        } else if state.contains(.disabled) {
            return disabledTitle
        } else if state.contains(.selected) {
            return selectedTitle
        } else {
            assertionFailure("invalid state")
            return nil
        }
    }

    public override final func setTitle(_ title: String?, for state: UIControl.State) {
        if state.contains(.normal) {
            normalTitle = title
        } else if state.contains(.highlighted) {
            highlightedTitle = title
        } else if state.contains(.disabled) {
            disabledTitle = title
        } else if state.contains(.selected) {
            selectedTitle = title
        } else {
            assertionFailure("invalid state")
        }
    }

    public override final func titleColor(for state: UIControl.State) -> UIColor? {
        assertionFailure("StyledButton must use TextStyle for title color")
        return nil
    }

    public override final func setTitleColor(_ color: UIColor?, for state: UIControl.State) {
        assertionFailure("StyledButton must use TextStyle for title color")
    }
}

// MARK: - Private Methods

extension StyledButton {
    private func updateStyles() {
        let normalTitle = self.normalTitle ?? ""

        func doUpdate(withText text: String, style: TextStyle, for state: UIControl.State) {
            let styledText = StyledString(string: text, style: style)
            super.setAttributedTitle(hideTitle ? nil : styledText.styledAttributedStringValue, for: state)
        }

        doUpdate(withText: normalTitle,
                 style: normalTextStyle,
                 for: .normal)
        doUpdate(withText: highlightedTitle ?? normalTitle,
                 style: highlightedTextStyle ?? normalTextStyle,
                 for: .highlighted)
        doUpdate(withText: disabledTitle ?? normalTitle,
                 style: disabledTextStyle ?? normalTextStyle,
                 for: .disabled)
        doUpdate(withText: selectedTitle ?? normalTitle,
                 style: selectedTextStyle ?? normalTextStyle,
                 for: .selected)
    }
}
