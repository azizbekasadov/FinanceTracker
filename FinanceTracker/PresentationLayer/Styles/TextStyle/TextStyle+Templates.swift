//
//  TextStyle+Templates.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

public extension TextStyle {
   struct headline {
       static var h0: TextStyle {
           return .init(font: .systemFont(ofSize: 24, weight: .bold), lineHeight: 24, kern: -0.5)
       }

       static var h1: TextStyle {
           return .init(font: .systemFont(ofSize: 22, weight: .bold), lineHeight: 26)
       }

       static var h2: TextStyle {
           return .init(font: .systemFont(ofSize: 18, weight: .bold), lineHeight: 20)
       }

       static var h3: TextStyle {
           return .init(font: .systemFont(ofSize: 18, weight: .semibold), lineHeight: 25)
       }

       static var h4: TextStyle {
           return .init(font: .systemFont(ofSize: 16, weight: .bold), lineHeight: 20)
       }

       static var h5: TextStyle {
           return .init(font: .systemFont(ofSize: 14, weight: .bold), lineHeight: 16, kern: -0.42)
       }

       static var h6: TextStyle {
           return .init(font: .systemFont(ofSize: 12, weight: .bold), lineHeight: 14, kern: -0.36)
       }

       static var subheading1: TextStyle {
           return .init(font: .systemFont(ofSize: 12, weight: .bold), lineHeight: 20)
       }

       static var subheading2: TextStyle {
           return .init(font: .systemFont(ofSize: 9, weight: .bold), lineHeight: 12)
       }

       static var subheading3: TextStyle {
           return .init(font: .systemFont(ofSize: 8, weight: .bold), lineHeight: 10)
       }

       static var subheading4: TextStyle {
           return .init(font: .systemFont(ofSize: 9, weight: .medium), lineHeight: 14)
       }
    }

    struct paragraph {
        static var p1Medium: TextStyle {
            return .init(font: .systemFont(ofSize: 18, weight: .medium), lineHeight: 20)
        }

        static var p2Regular: TextStyle {
            return .init(font: .systemFont(ofSize: 16, weight: .regular), lineHeight: 22, kern: -0.2)
        }

        static var p2Medium: TextStyle {
            return .init(font: .systemFont(ofSize: 16, weight: .medium), lineHeight: 22, kern: -0.2)
        }

        static var p2Semibold: TextStyle {
            return .init(font: .systemFont(ofSize: 16, weight: .semibold), lineHeight: 22)
        }

        static var p2Bold: TextStyle {
            return .init(font: .systemFont(ofSize: 16, weight: .bold), lineHeight: 22, kern: -0.2)
        }

        static var p3Thin: TextStyle {
            return .init(font: .systemFont(ofSize: 14, weight: .regular), lineHeight: 20)
        }

        static var p3Regular: TextStyle {
            return .init(font: .systemFont(ofSize: 14, weight: .medium), lineHeight: 22)
        }

        static var p3Semibold: TextStyle {
            return .init(font: .systemFont(ofSize: 14, weight: .semibold), lineHeight: 22)
        }

        static var p4Regular: TextStyle {
            return .init(font: .systemFont(ofSize: 13, weight: .medium), lineHeight: 20)
        }

        static var p4Medium: TextStyle {
            return .init(font: .systemFont(ofSize: 13, weight: .medium), lineHeight: 16)
        }

        static var p4Semibold: TextStyle {
            return .init(font: .systemFont(ofSize: 13, weight: .semibold), lineHeight: 18)
        }

        static var p4Bold: TextStyle {
            return .init(font: .systemFont(ofSize: 13, weight: .bold), lineHeight: 20)
        }

        static var p5Regular: TextStyle {
            return .init(font: .systemFont(ofSize: 13, weight: .medium), lineHeight: 16)
        }

        static var p6Semibold: TextStyle {
            return .init(font: .systemFont(ofSize: 11, weight: .semibold), lineHeight: 15)
        }

        static var p6Regular: TextStyle {
            return .init(font: .systemFont(ofSize: 11, weight: .medium), lineHeight: 16)
        }

        static var p7Medium: TextStyle {
            return .init(font: .systemFont(ofSize: 10, weight: .medium), lineHeight: 14)
        }

        static var p8Semibold: TextStyle {
            return .init(font: .systemFont(ofSize: 9, weight: .semibold), lineHeight: 13)
        }

        static var p8CrossedMedium: TextStyle {
            return .init(font: .systemFont(ofSize: 9, weight: .semibold), lineHeight: 16, strikethrough: true)
        }

        static var longText: TextStyle {
            return .init(font: .systemFont(ofSize: 15, weight: .regular), lineHeight: 22, kern: 1)
        }
    }

    struct buttons {
        static var buttonBig: TextStyle {
            return .init(font: .systemFont(ofSize: 16, weight: .bold), lineHeight: 22)
        }

        static var buttonSmall: TextStyle {
            return .init(font: .systemFont(ofSize: 12, weight: .semibold), lineHeight: 22)
        }

        static var textButtonBig: TextStyle {
            return .init(font: .systemFont(ofSize: 16, weight: .medium), lineHeight: 22)
        }

        static var textButtonSmall: TextStyle {
            return .init(font: .systemFont(ofSize: 13, weight: .semibold), lineHeight: 22)
        }
    }

    struct elements {
        static var tabsBig: TextStyle {
            return .init(font: .systemFont(ofSize: 11, weight: .bold), lineHeight: 12)
        }

        static var tabsSmall: TextStyle {
            return .init(font: .systemFont(ofSize: 9, weight: .bold), lineHeight: 12)
        }

        static var labels: TextStyle {
            return .init(font: .systemFont(ofSize: 12, weight: .bold), lineHeight: 9)
        }

        static var searchInputPlaceholder: TextStyle {
            return .init(font: .systemFont(ofSize: 14, weight: .regular), lineHeight: 18, kern: -0.2)
        }
    }

    struct table {
        static var t1: TextStyle {
            return .init(font: .systemFont(ofSize: 14, weight: .medium), lineHeight: 25, kern: -0.2)
        }

        static var t2: TextStyle {
            return .init(font: .systemFont(ofSize: 14, weight: .regular), lineHeight: 25, kern: -0.2)
        }

        static var t3: TextStyle {
            return .init(font: .systemFont(ofSize: 14, weight: .bold), lineHeight: 25, kern: -0.2)
        }
    }

    // MARK: -

    /// Стиль по умолчанию если что-то пошло не так.
    static var unspecified: TextStyle {
        return .init(font: .systemFont(ofSize: 80), lineHeight: 80)
    }
}
