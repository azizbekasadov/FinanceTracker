//
//  TextStyle.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 29/12/22.
//

import UIKit

/// Стиль текста который забит в фигме.
public struct TextStyle {
    /// Параметры по умолчанию, использующиеся если не задан параметр.
    private struct DefaultTextStyleParameter {
        static let lineBreakMode: NSLineBreakMode = .byTruncatingTail
    }

    /// Шрифт, размер
    private let font: UIFont

    /// Межстрочный интервал из фигмы
    private let lineHeight: CGFloat

    /// Цвет
    private let color: UIColor?

    /// Способ переноса текста
    private let lineBreakMode: NSLineBreakMode?

    /// Зачёркнутость
    private let strikethrough: Bool?

    /// Подчёркнутость
    private let underlined: Bool?

    /// Межбуквенный интервал
    private let kern: CGFloat?

    /// Выравнивание
    private let alignment: NSTextAlignment?

    // MARK: Init

    public init(font: UIFont,
         lineHeight: CGFloat) {
        self.font = font
        self.lineHeight = lineHeight
        self.color = nil
        self.lineBreakMode = nil
        self.strikethrough = nil
        self.underlined = nil
        self.kern = nil
        self.alignment = nil
    }

    public init(font: UIFont,
         lineHeight: CGFloat,
         color: UIColor? = nil,
         lineBreakMode: NSLineBreakMode? = nil,
         strikethrough: Bool? = nil,
         underlined: Bool? = nil,
         kern: CGFloat? = nil,
         alignment: NSTextAlignment? = nil) {
        self.font = font
        self.lineHeight = lineHeight
        self.color = color
        self.lineBreakMode = lineBreakMode
        self.strikethrough = strikethrough
        self.underlined = underlined
        self.kern = kern
        self.alignment = alignment
    }

    // MARK: Modificators

    public func with(font: UIFont? = nil,
              lineHeight: CGFloat? = nil,
              color: UIColor? = nil,
              lineBreakMode: NSLineBreakMode? = nil,
              strikethrough: Bool? = nil,
              underlined: Bool? = nil,
              kern: CGFloat? = nil,
              alignment: NSTextAlignment? = nil) -> TextStyle {
        return TextStyle(font: font ?? self.font,
                         lineHeight: lineHeight ?? self.lineHeight,
                         color: color ?? self.color,
                         lineBreakMode: lineBreakMode ?? self.lineBreakMode,
                         strikethrough: strikethrough ?? self.strikethrough,
                         underlined: underlined ?? self.underlined,
                         kern: kern ?? self.kern,
                         alignment: alignment ?? self.alignment)
    }

    /// Для удобства доступа.
    public func with(color: UIColor) -> TextStyle {
        return with(font: nil, lineHeight: nil, color: color, lineBreakMode: nil, strikethrough: nil, underlined: nil, kern: nil, alignment: nil)
    }

    /// Для удобства доступа.
    public func with(lineBreakMode: NSLineBreakMode) -> TextStyle {
        return with(font: nil, lineHeight: nil, color: nil, lineBreakMode: lineBreakMode, strikethrough: nil, underlined: nil, kern: nil, alignment: nil)
    }

    /// Для удобства доступа.
    public func with(font: UIFont) -> TextStyle {
        return with(font: font, lineHeight: nil, color: nil, lineBreakMode: nil, strikethrough: nil, underlined: nil, kern: nil, alignment: nil)
    }

    /// Для удобства доступа.
    public func with(alignment: NSTextAlignment) -> TextStyle {
        return with(font: nil, lineHeight: nil, color: nil, lineBreakMode: nil, strikethrough: nil, underlined: nil, kern: nil, alignment: alignment)
    }

    // MARK: Attributes

    public  var attributes: [NSAttributedString.Key: Any] {
        var attributes: [NSAttributedString.Key: Any] = [:]
        attributes[.font] = font

        if let color = color {
            attributes[.foregroundColor] = color
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = lineHeight
        paragraphStyle.maximumLineHeight = lineHeight

        // Необходимо для правильного центрирования текста
        let baselineOffset: CGFloat = (lineHeight - font.lineHeight) / 4.0
        attributes[.baselineOffset] = baselineOffset

        // Если высота строк не будет соответствовать дизайну, глянуть:
        // https://stackoverflow.com/questions/68229689/fix-line-spacing-in-custom-font-in-swiftui
        // https://github.com/surfstudio/iOS-Utils/issues/14

        if let lineBreakMode = lineBreakMode {
            paragraphStyle.lineBreakMode = lineBreakMode
        } else {
            paragraphStyle.lineBreakMode = DefaultTextStyleParameter.lineBreakMode
        }
        if let alignment = alignment {
            paragraphStyle.alignment = alignment
        }
        attributes[.paragraphStyle] = paragraphStyle

        if let strikethrough = strikethrough,
           strikethrough == true {
            attributes[.strikethroughStyle] = NSUnderlineStyle.single.rawValue
        }

        if let underlined = underlined,
           underlined == true {
            attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
            if let color = color {
                attributes[.underlineColor] = color
            }
        }

        if let kern = kern {
            attributes[NSAttributedString.Key.kern] = kern
        }

        return attributes
    }
}
