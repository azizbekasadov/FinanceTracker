//
//  UIButton+Tappable.swift
//  FinanceTracker
//
//  Created by Azizbek Asadov on 26/12/22.
//

import UIKit

// MARK: - Tappabale
private var kButtonBlockAssociationKey: UInt8 = 0

extension UIButton {
    private class ClosureWrapper {
        var closure: EmptyClosure?

        init(_ closure: EmptyClosure?) {
            self.closure = closure
        }
    }

    var tapButtonBlock: EmptyClosure? {
        get {
            if let cw = objc_getAssociatedObject(self,
                                                 &kButtonBlockAssociationKey) as? ClosureWrapper {
                return cw.closure
            }
            return nil
        }
        set(newValue) {
            objc_setAssociatedObject(self,
                                     &kButtonBlockAssociationKey,
                                     ClosureWrapper(newValue),
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    /** Links UIButton tap (TouchUpInside) event to a block.

     Example Usage:

     ```
     button.tap {
     // do something
     }
     ```

     Or
     ```
     button.tap(doSomething)

     // later
     func doSomething() {
     // ...
     }
     ```

     - Returns: Itself for chaining purposes

     */
    @discardableResult
    func onTap(_ block: @escaping EmptyClosure) -> UIButton {
        addTarget(self, action: #selector(UIButton.hasTapped), for: .touchUpInside)
        tapButtonBlock = block
        return self
    }

    /** */
    @objc
    private func hasTapped() {
        tapButtonBlock?()
    }
}
